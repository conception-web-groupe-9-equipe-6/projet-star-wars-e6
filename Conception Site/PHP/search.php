<?php
session_start();

    $servername = "127.0.0.1";
	$username = "grp_9_6";
	$password = "Iez8geih4o";
	$dbname = "bdd_9_6";

try {
  $conn = new PDO("mysql:host=$servername;dbname=$dbname", $username, $password);
  // set the PDO error mode to exception
  $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
 
} catch(PDOException $e) {
  echo "Connection failed: " . $e->getMessage();
}
?>

<!DOCTYPE html>
<html lang="fr">

<head>
     <title>Star Wars Factory</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z" crossorigin="anonymous">
    <link rel="stylesheet" href="CSS/style.css"> <!--renvoie au css nommé style.css-->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
</head>

<body>
<main role="main">
    <?php include 'PHP/baniere.php'; ?>

    <?php include 'PHP/navbar.php'; ?>

  <?php

    $stmt = $conn->prepare("SELECT id, name FROM People WHERE name = '".$_GET['word']."' ");
    $stmt->execute();

    // set the resulting array to associative
    $result = $stmt->setFetchMode(PDO::FETCH_ASSOC);
  
    foreach($stmt as $value) {
      $count = 0;
      foreach($value as $v) {
      $people[$count] = $v;
      $count=$count+1;
      }
    }

    $stmt = $conn->prepare("SELECT id, title FROM Film WHERE title = '".$_GET['word']."' ");
    $stmt->execute();

    // set the resulting array to associative
    $result = $stmt->setFetchMode(PDO::FETCH_ASSOC);
  
    foreach($stmt as $value) {
      $count = 0;
      foreach($value as $v) {
        $film[$count] = $v;
        $count=$count+1;
      }
    }

    $stmt = $conn->prepare("SELECT id, name FROM Planet WHERE name = '".$_GET['word']."' ");
    $stmt->execute();

    // set the resulting array to associative
    $result = $stmt->setFetchMode(PDO::FETCH_ASSOC);
    foreach($stmt as $value){
      $count=0;
      foreach($value as $v){
        $planet[$count] = $v;
        $count=$count+1;
      }
    }

    $stmt = $conn->prepare("SELECT id, name FROM Species WHERE name = '".$_GET['word']."' ");
    $stmt->execute();

    // set the resulting array to associative
    $result = $stmt->setFetchMode(PDO::FETCH_ASSOC);
    foreach($stmt as $value) {
      $count = 0;
      foreach($value as $v){
        $species[$count] = $v;
        $count=$count+1;
      }
    }

    $stmt = $conn->prepare("SELECT id, class FROM Starship WHERE class = '".$_GET['word']."' ");
    $stmt->execute();

    // set the resulting array to associative
    $result = $stmt->setFetchMode(PDO::FETCH_ASSOC);
    foreach($stmt as $value) {
      $count = 0;
      foreach($value as $v){
        $starship[$count] =$v;
        $count=$count+1;
      }
    }

    echo'
    <div class="couleur m-3 p-5"> 
      <h1>Resultat :</h>
    </div>
    ';

    if(empty($people)){

    }else{
      echo'<div class ="text-white m-4">
       <h2>Personnages : </h>
       </div>
       <div class="m-5">
       <a href="people.php?id=';echo $people[0];echo'">
       <p>';echo $people[1];echo'</p>
       </a>
       </div>
       ';
    }

    if(empty($film)){

    }else{
      echo'<div class ="text-white m-4">
       <h2>Films : </h>
       </div>
       <div class="m-5">
       <a href="film.php?id=';echo $film[0];echo'">
       <p>';echo $film[1];echo'</p>
       </a>
       </div>
       ';
    }

    if(empty($planet)){

    }else{
      echo'<div class ="text-white m-4">
       <h2>Planets : </h>
       </div>
       <div class="m-5">
       <a href="planet.php?id=';echo $planet[0];echo'">
       <p>';echo $planet[1];echo'</p>
       </a>
       </div>
       ';
    }

    if(empty($species)){

    }else{
      echo'<div class ="text-white m-4">
       <h2>Species : </h>
       </div>
       <div class="m-5">
       <a href="Species.php">
       <p>';echo $species[1];echo'</p>
       </a>
       </div>
       ';
    }

    if(empty($starship)){

    }else{
      echo'<div class ="text-white m-4">
       <h2>Starships : </h>
       </div>
       <div class="m-5">
       <a href="Starship.php">
       <p>';echo $starship[1];echo'</p>
       </a>
       </div>
       ';
    }

    ?>
</main>
</body>
</html>
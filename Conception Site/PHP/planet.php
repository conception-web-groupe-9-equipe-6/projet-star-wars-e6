<?php
session_start();

$servername = "127.0.0.1";
$username = "grp_9_6";
$password = "Iez8geih4o";
$dbname = "bdd_9_6";

try {
  $conn = new PDO("mysql:host=$servername;dbname=$dbname", $username, $password);
  // set the PDO error mode to exception
  $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
} catch (PDOException $e) {
  echo "Connection failed: " . $e->getMessage();
}
?>

<!DOCTYPE html>
<html lang="fr">

<head>
  <title>Star Wars Factory</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z" crossorigin="anonymous">
  <link rel="stylesheet" href="CSS/style.css">
  <!--renvoie au css nommé style.css-->
  <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
  <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
</head>

<body>
  <main role="main">
    <?php include 'PHP/baniere.php'; ?>

    <?php include 'PHP/navbar.php'; ?>

    <?php

    $stmt = $conn->prepare("SELECT name, diameter, population, description FROM Planet WHERE id = '" . $_GET['id'] . "' ");
    $stmt->execute();

    // set the resulting array to associative
    $result = $stmt->setFetchMode(PDO::FETCH_ASSOC);
    foreach ($stmt as $value) {
      $count = 0;
      foreach ($value as $v) {
        $planet[$count] = $v;
        $count = $count + 1;
      }
    }

    $stmt = $conn->prepare("SELECT id_film FROM FilmPlanets WHERE id_planet = '" . $_GET['id'] . "' ");
    $stmt->execute();

    // set the resulting array to associative
    $result = $stmt->setFetchMode(PDO::FETCH_ASSOC);
    foreach ($stmt as $value) {
      $count = 0;
      foreach ($value as $v) {
        $films[$count] = $v;
        $count = $count + 1;
      }
    }

    $stmt = $conn->prepare("SELECT id_climate FROM HasClimate WHERE id_planet = '" . $_GET['id'] . "' ");
    $stmt->execute();

    // set the resulting array to associative
    $result = $stmt->setFetchMode(PDO::FETCH_ASSOC);
    foreach ($stmt as $value) {
      $count = 0;
      foreach ($value as $v) {
        $climates[$count] = $v;
        $count = $count + 1;
      }
    }

    echo '
    <div class="row text-white m-5">
        <div class="col-md-1.5 card mb-4 box-shadow bg-dark"">
             <img src="IMG/Planet/';
    echo $_GET['id'];
    echo '.jpg" alt="Avatar" style
             ="width:300px;height:300px;">
        </div>
        <div class="col-md-1" style="text-align:center">
             <table >
                <br /><br /><br />
                <tr>
                    <p>Diameter: <b>';
    echo $planet[1];
    echo 'km</b></p>
                </tr>
                <tr>
                    <p>Population: <b>';
    echo $planet[2];
    echo '</b></p>
                </tr>
                <tr>
                <p>Possèdent les climats suivants:</br>';
    foreach ($climates as $v) {
      $stmt = $conn->prepare("SELECT name FROM Climate WHERE id = '" . $v . "' ");
      $stmt->execute();

      // set the resulting array to associative
      $result = $stmt->setFetchMode(PDO::FETCH_ASSOC);
      foreach ($stmt as $value) {
        foreach ($value as $v) {
          echo $v;
          echo ' ';
        }
      }
    }
    echo '
                </p>
                </tr>
            </table>
        </div>
        <div class="col-md-8 text-white">
            <h1><b>';
    echo $planet[0];
    echo '</b></h1>
            <p><br />';
    echo $planet[3];
    echo '<br /></p>
            <p2>Sources:Wikipedia.<br /></p>
        </div>
    </div>';
    echo '</br></br>
    <div class="couleur"> 
      <h1>Apparait dans les films :</h1>
  </div>

        <div class="container py-5">

            <div class="row">';
    $stmt = $conn->prepare("SELECT id_film FROM FilmPlanets WHERE id_planet = '" . $_GET['id'] . "' ");
    $stmt->execute();

    // set the resulting array to associative
    $result = $stmt->setFetchMode(PDO::FETCH_ASSOC);
    foreach ($stmt as $value) {
      foreach ($value as $v) {
        echo '
            <div class="col-md-2">
              <a href="film.php?id=';
        echo $v;
        echo '">
                    <div class="card mb-2 box-shadow bg-dark">
                        <img class="card"
                              data-src="holder.js/400px225?theme=thumb&amp;bg=55595c&amp;fg=eceeef&amp;text=Thumbnail" alt="Thumbnail [400x225]" src="IMG/Film/';
        echo $v;
        echo '.jpg" data-holder-rendered="true" style="height: 225px; width: 400; display: block;">
                     </div>
                </a>
            </div>';
      }
    }
    echo '
        </div>

    </div>';

    ?>
  </main>
</body>

</html>
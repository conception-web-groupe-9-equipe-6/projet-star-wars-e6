<blockquote class="blockquote">
    <br>
    <p class="mb-0">Star Wars is an American epic space opera media franchise created by George Lucas, which began with the eponymous 1977 film and quickly became a worldwide pop-culture phenomenon.
        The franchise has been expanded into various films and other media, including television series, video games, novels, comic books, theme park attractions, and themed areas, comprising an all-encompassing fictional universe.
        The franchise holds a Guinness World Records title for the "Most successful film merchandising franchise.
        "In 2020, the Star Wars franchise's total value was estimated at US$70 billion, and it is currently the fifth-highest-grossing media franchise of all time.

        The original film, retroactively subtitled
    </p>
    <br>
    <p class="mb-0">
        <ul>
            <li>Episode IV: A New Hope, was followed by the sequels</li>
            <li>Episode V: The Empire Strikes Back (1980)</li>
            <li>Episode VI: Return of the Jedi (1983), forming the original Star Wars trilogy.</li>
        </ul>
        A prequel trilogy was later released, consisting of :
    </p>
    <ul>
        <li>Episode I: The Phantom Menace (1999), </li>
        <li>Episode II: Attack of the Clones (2002), </li>
        <li>Episode III: Revenge of the Sith (2005).</li>
    </ul>
    <p class="mb-0">
        In 2012, Lucas sold his production company to Disney, relinquishing his ownership of the franchise.
        The subsequently produced sequel trilogy consists of :
    </p>
    <br>
    <ul>
        <li>Episode VII: The Force Awakens (2015),</li>
        <li>Episode VIII: The Last Jedi (2017),</li>
        <li>Episode IX: The Rise of Skywalker (2019).</li>
    </ul>
    <p class="mb-0">
        Together, the three trilogies form what has been referred to as the "Skywalker saga".
        All nine films were nominated for Academy Awards (with wins going to the first two released) and were commercially successful.
        Together with the theatrical spin-off films :
    </p>
    <br>
    <ul>
        <li>Rogue One (2016)</li>
        <li>Solo: A Star Wars Story (2018),</li>
    </ul>
    <p class="mb-0">
        the combined box office revenue of the films equates to over US$10 billion, and it is currently the second-highest-grossing film franchise.
    </p>
</blockquote>
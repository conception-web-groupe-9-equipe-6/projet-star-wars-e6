<?php

session_start();

$servername = "localhost";
$username = "grp_9_6";
$password = "Iez8geih4o";
$dbname = "bdd_9_6";

try {
    $conn = new PDO("mysql:host=$servername;dbname=$dbname", $username, $password);
    // set the PDO error mode to exception
    $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
} catch (PDOException $e) {
    echo "Connection failed: " . $e->getMessage();
}

$stmt = $conn->prepare("SELECT username, hash FROM users WHERE username = '" . $_POST['username'] . "' ");
$stmt->execute();

$result = $stmt->fetchAll(PDO::FETCH_ASSOC);

if(count($result) != 1){
    echo "plusieurs utilisateurs ont cet username";
    header("Location: index.php");
    return;
}

$hash = hash("sha512", $_POST['pw'], false);

if($hash == $result[0]['hash']){
    $_SESSION["username"] = $_POST['username'];
}

header("Location: index.php");

?>
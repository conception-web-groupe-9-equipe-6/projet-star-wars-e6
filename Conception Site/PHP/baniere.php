<div class="container-fluid">
    <div style="background:black;" class="row">
        <div class="col">
            <div class="row">
                <div class="col text-right">
                    <!-- Modal -->

                    <?php if (!isset($_SESSION["username"])) : ?>
                        <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#inscription">
                            S'inscrire
                        </button>
                        <div class="modal fade" id="inscription" tabindex="-1" role="dialog" aria-labelledby="inscription" aria-hidden="true">
                            <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h5 class="modal-title" id="inscription">Inscription</h5>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                    <div class="modal-body">
                                        <form action="register.php" method="post">
                                            <div class="form-group">
                                                <label for="username">Username:</label>
                                                <input type="username" class="form-control" placeholder="Entrez votre nom d utilisateur" name="username" id="username">
                                            </div>
                                            <div class="form-group">
                                                <label for="email">Email:</label>
                                                <input type="email" class="form-control" placeholder="Entrez votre Email" name="email" id="email">
                                            </div>
                                            <div class="form-group">
                                                <label for="password">Password:</label>
                                                <input type="password" class="form-control" placeholder="Entrez votre mot de passe" name="pw" id="pw">
                                            </div>
                                            <button type="submit" id="connectionbtn" name="formconnection" class="btn btn-primary">S inscrire</button>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#Connection">
                            Se connecter
                        </button>
                        <div class="modal fade" id="Connection" tabindex="-1" role="dialog" aria-labelledby="Connection" aria-hidden="true">
                            <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h5 class="modal-title" id="Connection">Connection</h5>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                    <div class="modal-body">
                                        <form action="login.php" method="post">
                                            <div class="form-group">
                                                <label for="username">Username:</label>
                                                <input type="username" class="form-control" placeholder="Entrez votre nom d utilisateur" name="username" id="username">
                                            </div>
                                            <div class="form-group">
                                                <label for="password">Password:</label>
                                                <input type="password" class="form-control" placeholder="Entrez votre mot de passe" name="pw" id="pw">
                                            </div>
                                            <button type="submit" id="connectionbtn" name="formconnection" class="btn btn-primary">Se connecter</button>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    <?php else : ?>
                        <button onclick="location.href='disconnect.php'" type="button" class="btn btn-primary" data-toggle="modal" data-target="#Disconnect">
                            Se deconnecter
                        </button>
                    <?php endif; ?>

                </div>
            </div>
            <table>
                <ti>
                    <div class="LOGO">
                        <td><img src="IMG/LOGO.png" alt="" width="100%" height="auto"></td>
                        <td><img src="IMG/BAN.png" alt="" width="100%" height="auto"></td>
                    </div>
                </ti>
            </table>
        </div>
    </div>
</div>
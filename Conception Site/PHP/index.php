<?php
$episode = "Episode IV";
$titre = "Un nouvel espoir";
$desc = "dedans y'a dark vador je crois";

session_start();

$servername = "localhost";
$username = "grp_9_6";
$password = "Iez8geih4o";
$dbname = "bdd_9_6";

try {
    $conn = new PDO("mysql:host=$servername;dbname=$dbname", $username, $password);
    // set the PDO error mode to exception
    $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
} catch (PDOException $e) {
    echo "Connection failed: " . $e->getMessage();
}
?>

<!DOCTYPE html>
<html lang="fr">

<head>
    <title>Star Wars Factory</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z" crossorigin="anonymous">
    <link rel="stylesheet" href="CSS/style.css">

    <link rel="icon" href="favicon.ico">
    <!--renvoie au css nommé style.css-->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
    <script src="JS/bootstrap.min.js"></script>
</head>

<body>
    <main role="main">

        <?php include 'PHP/baniere.php'; ?>

        <?php include 'PHP/navbar.php'; ?>

        <div class="container bg-light">
            <br>
            <blockquote class="blockquote text-center">
                <p class="mb-0">Classement des trois meilleurs films</p>
                <footer class="blockquote-footer">Par la communauté </footer>
            </blockquote>
            <br>
            <div class="container bg-dark">
                <br>
                <div class="row">
                    <?php include 'PHP/card.php'; ?>
                    <?php include 'PHP/card.php'; ?>
                    <?php include 'PHP/card.php'; ?>
                </div>
                <br>
            </div>

            <div class="container bg-light">
                <div class="container-fluid">
                    <?php include 'text.php'; ?>
                </div>
                <div class="row">
                    <br>
                    <?php include 'PHP/front.php'; ?>
<br>
                </div>


            </div>



        </div>
            
        </div>

    </div>
    </main>
<div class="container text-center" style="color:white">
			<div class="row">
				<div class="col"><br>
<div class="footer-copyright text-center py-3">
			<p id="copyright" style="color:white">copyright ESIGELEC : LIN & MATHOREL</p>
		</div>

							</div>


</body>

</html>
<?php
session_start();

$servername = "127.0.0.1";
$username = "grp_9_6";
$password = "Iez8geih4o";
$dbname = "bdd_9_6";

try {
  $conn = new PDO("mysql:host=$servername;dbname=$dbname", $username, $password);
  // set the PDO error mode to exception
  $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
} catch (PDOException $e) {
  echo "Connection failed: " . $e->getMessage();
}
?>

<!DOCTYPE html>
<html lang="fr">

<head>
  <title>Star Wars Factory</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z" crossorigin="anonymous">
  <link rel="stylesheet" href="CSS/style.css">
  <!--renvoie au css nommé style.css-->
  <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
  <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
</head>

<body>
  <main role="main">
    <?php include 'PHP/baniere.php'; ?>

    <?php include 'PHP/navbar.php'; ?>

    <?php

    $stmt = $conn->prepare("SELECT title, release_date, episode, opening FROM Film WHERE id = '" . $_GET['id'] . "' ");
    $stmt->execute();

    // set the resulting array to associative
    $result = $stmt->setFetchMode(PDO::FETCH_ASSOC);

    foreach ($stmt as $value) {
      $count = 0;
      foreach ($value as $v) {
        $film[$count] = $v;
        $count = $count + 1;
      }
    }

    echo '
    <div class="row m-5" style="text-align:center">
        <div class="col-md-9 text-white" style="text-align:left">
            <div class="couleur"> 
             <h2>Synopsis :</h1>
            </div>
            <p><br />';
    echo $film[3];
    echo '<br /></p>

            <div class="couleur"> 
             <h2>Personnages qui apparaient dans ce film :</h>
            </div>

             <div class="container py-5">

              <div class="row">';
    $stmt = $conn->prepare("SELECT id_people FROM PlaysIn WHERE id_film = '" . $_GET['id'] . "' ");
    $stmt->execute();

    // set the resulting array to associative
    $result = $stmt->setFetchMode(PDO::FETCH_ASSOC);
    foreach ($stmt as $value) {
      $count = 0;
      foreach ($value as $v) {
        echo '
                  <div class="col-md-2">
                   <a href="people.php?id=';
        echo $v;
        echo '">
                    <div class="card mb-2 box-shadow bg-dark">
                        <img class="card"
                              data-src="holder.js/100px400?theme=thumb&amp;bg=55595c&amp;fg=eceeef&amp;text=Thumbnail" alt="Thumbnail [100x400]" src="IMG/People/';
        echo $v;
        echo '.jpg" data-holder-rendered="true" style="height: 100px; width: 400; display: block;">
                        <div class="card-header bg-dark">
                        <font size ="-1">';
        $stm = $conn->prepare("SELECT name FROM People WHERE id = '" . $v . "' ");
        $stm->execute();

        // set the resulting array to associative
        $result = $stmt->setFetchMode(PDO::FETCH_ASSOC);
        foreach ($stm as $name) {
          $count = 0;
          foreach ($name as $n) {
            if ($count == 0) {
              echo '
                              
                              <p class="card-text text-white" style="text-align:center">';
              echo $n;
              echo '</p>';

              $count = $count + 1;
            }
          }
        }
        echo '
                        </font>
                         </div>
                    </div>
                   </a>
                  </div>';
      }
    }
    echo '
             </div>
            </div>
        </div>
        <div class="card" style="width: 18rem;">
          <div class="card-header">
            <b>';
    echo $film[0];
    echo '</b>
          </div>
          <img src="IMG/Film/';
    echo $_GET['id'];
    echo '.jpg" class="card-img-top" alt="...">
          <ul class="list-group list-group-flush">
            <li class="list-group-item">Release date: <b>';
    echo $film[1];
    echo '</b></li>
            <li class="list-group-item">Episode: <b>';
    echo $film[2];
    echo '</b></li>
          </ul>
          <div class="row card-header">
          <div class="col-md-6">';
    if ($_GET['id'] != 1) {
      echo '<a href="film.php?id=';
      echo $_GET['id'] - 1;
      echo '" class="card-link">Derniere Episode</a>';
    } else {
      echo '<p>Premier film sortie</p>';
    }
    echo '
          </div>
          <div class="col-md-6">';
    if ($_GET['id'] != 6) {
      echo '<a href="film.php?id=';
      echo $_GET['id'] + 1;
      echo '" class="card-link">Prochaine Episode</a>';
    } else {
      echo '<p>Dernier film sortie</p>';
    }
    echo '
          </div>
          <font size="-3">*Film dans ordre chronologique de sortie</font>
          </div>
          </div>
        </div>
    </div>';
    echo '
    <div class="row m-5" style="text-align:center">
        <div class="col-md-9 text-white" style="text-align:left">
            <div class="couleur"> 
             <h2>Planets qui apparaient dans ce film :</h>
            </div>

             <div class="container py-5">

              <div class="row">';
    $stmt = $conn->prepare("SELECT id_planet FROM FilmPlanets WHERE id_film = '" . $_GET['id'] . "' ");
    $stmt->execute();

    // set the resulting array to associative
    $result = $stmt->setFetchMode(PDO::FETCH_ASSOC);
    foreach ($stmt as $value) {
      $count = 0;
      foreach ($value as $v) {
        echo '
                  <div class="col-md-3">
                   <a href="planet.php?id=';
        echo $v;
        echo '">
                    <div class="card mb-3 box-shadow bg-dark">
                        <img class="card"
                              data-src="holder.js/100px400?theme=thumb&amp;bg=55595c&amp;fg=eceeef&amp;text=Thumbnail" alt="Thumbnail [100x400]" src="IMG/Planet/';
        echo $v;
        echo '.jpg" data-holder-rendered="true" style="height: 150px; width: 400; display: block;">
                        <div class="card-header bg-dark">
                        <font size ="-1">';
        $stm = $conn->prepare("SELECT name FROM Planet WHERE id = '" . $v . "' ");
        $stm->execute();

        // set the resulting array to associative
        $result = $stmt->setFetchMode(PDO::FETCH_ASSOC);
        foreach ($stm as $name) {
          $count = 0;
          foreach ($name as $n) {
            if ($count == 0) {
              echo '
                              
                              <p class="card-text text-white" style="text-align:center">';
              echo $n;
              echo '</p>';

              $count = $count + 1;
            }
          }
        }
        echo '
                        </font>
                         </div>
                    </div>
                   </a>
                  </div>';
      }
    }
    echo '
             </div>
            </div>
        </div>';

/*
<div class="container">
  <span id="rateMe2"  class="empty-stars"></span>
</div>

<!-- rating.js file -->
<script src="js/addons/rating.js"></script>
*/

    ?>

  </main>
</body>

</html>


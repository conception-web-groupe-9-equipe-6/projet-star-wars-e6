<div id="demo" class="carousel slide carousel-fade" data-ride="carousel">
    <!-- Indicateurs -->
    <ul class="carousel-indicators">
        <li data-target="#demo" data-slide-to="0" class=""></li>
        <li data-target="#demo" data-slide-to="1" class="active"></li>
        <li data-target="#demo" data-slide-to="2" class=""></li>
    </ul>

    <div class="carousel-inner">
        <div class="carousel-item" data-interval="4000">
                <img src="IMG/dest.jpg" alt="Croiseur Executor" class="d-block w-100">
            <div class="carousel-caption d-none d-md-block"><a href="Starship.php">
                    <h4>Vaisseaux</h4>
                </a>
            </div>
        </div>
        <div class="carousel-item active" data-interval="4000">
                <img src="IMG/tat.jpg" alt="Mustafar" class="d-block w-100">
            <div class="carousel-caption d-none d-md-block"><a href="#">
                    <h4>Planètes</h4>
                </a>
            </div>
        </div>
        <div class="carousel-item" data-interval="4000">
                <img src="IMG/dark.jpg" alt="Anakin Skywalker" class="d-block w-100">
            <div class="carousel-caption d-none d-md-block"><a href="Personnages.php">
                    <h4>Personnages</h4>
                </a>
            </div>
        </div>
    </div>

    <a class="carousel-control-prev" href="#demo" role="button" data-slide="prev">
        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
        <span class="sr-only">Précédent</span>
    </a>
    <a class="carousel-control-next" href="#demo" role="button" data-slide="next">
        <span class="carousel-control-next-icon" aria-hidden="true"></span>
        <span class="sr-only">Suivant</span>
    </a>
</div>
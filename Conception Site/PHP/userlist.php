<?php
session_start();

    $servername = "127.0.0.1";
	$username = "grp_9_6";
	$password = "Iez8geih4o";
	$dbname = "bdd_9_6";

try {
  $conn = new PDO("mysql:host=$servername;dbname=$dbname", $username, $password);
  // set the PDO error mode to exception
  $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
 
} catch(PDOException $e) {
  echo "Connection failed: " . $e->getMessage();
}
?>

<!DOCTYPE html>
<html lang="fr">

<head>
     <title>Star Wars Factory</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z" crossorigin="anonymous">
    <link rel="stylesheet" href="CSS/style.css"> <!--renvoie au css nommé style.css-->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
</head>

<body>
<main role="main">
    <?php include 'PHP/baniere.php'; ?>

    <?php include 'PHP/navbar.php'; ?>
  </br></br>

  <?php

    echo'
    <table class="table table-dark text-white p-5 border border-dark">
      <thead>
        <tr>
          <th scope="col">#</th>
          <th scope="col">Username</th>
          <th scope="col">Email</th>
          <th scope="col"></th>
        </tr>
      </thead>
      <tbody>';
        $stmt = $conn->prepare("SELECT id, username, email FROM users WHERE admin = 0 ");
        $stmt->execute();

        // set the resulting array to associative
        $result = $stmt->setFetchMode(PDO::FETCH_ASSOC);
      
        foreach($stmt as $value) {
          $count = 0;
          foreach($value as $v) {
            $users[$count] = $v;
            $count=$count+1;
          }
          
          echo'
          <tr>
            <th scope="row">';echo $users[0]; echo'</th>
            <td>';echo $users[1]; echo '</td>
            <td>';echo $users[2]; echo '</td>
            <td>
              <button type="button" class="btn btn-outline-secondary btn-sm m-0 waves-effect" data-toggle="modal" data-target="#supprimer';echo $users[0]; echo '">
                Supprimer
              </button>

              <div class="modal" id="supprimer';echo $users[0]; echo'" tabindex="-1" role="dialog" aria-labelledby="supprimer" aria-hidden="true">
                <div class="modal-dialog" role="document">
                  <div class="modal-content">
                    <div class="modal-header">
                      <h5 class="text-dark">Etes vous sure de supprimer ';echo $users[1]; echo'?</h5>
                    </div>
                    <div class="modal-body">
                      <p class="text-dark">Vous aller supprimer ';echo $users[1]; echo' et cela est irreversible!!! Etes vous vraiment sure!?</p>
                    </div>
                    <div class="modal-footer">
                      <button type="button" class="btn btn-outline-secondary m-0 waves-effect" data-dismiss="modal">Close</button>
                      <form action="userdel.php" method="POST">
                        <input type= "hidden" name="id" value="';echo $users[0]; echo'" />
                        <input type="submit" class="btn btn-outline-primary m-0 waves-effect" role="button" aria-pressed="true" value="Supprimer" />
                      </form>
                    </div>
                  </div>
                </div>
              </div>
              
          </tr>';
        }
        echo'
      </tbody>
    </table>
    ';

    ?>
</main>
</body>
</html>